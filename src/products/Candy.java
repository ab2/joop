package products;

import java.util.Date;
import main.Recipe;


public class Candy {

  private Date productionTime;
  private Boolean isWrapped;
  Recipe recipe;

  public Candy() {
    this.productionTime = new Date();
  }

  public Candy(Recipe recipe) {
    this.productionTime = new Date();
    this.recipe = recipe;
  }

  public Date getProductionTime() {
    return productionTime;
  }

  public void setProductionTime(Date productionTime) {
    this.productionTime = productionTime;
  }

  public Recipe getRecipe() {
    return recipe;
  }

  public void wrap() {
    this.isWrapped = true;
  }
}
