package products;

import main.Recipe;


public class Medallion extends Candy {

  private String picture;

  public Medallion() {
    super(Recipe.medallionRecipe());
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }
}
