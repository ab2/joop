package products;

import main.Recipe;


public class RecipeCandy extends Candy {

  public RecipeCandy() {
    super();
  }

  public void setRecipe(Recipe recipe) {
    this.recipe = recipe;
  }
}
