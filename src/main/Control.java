package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import products.Candy;
import products.Medallion;
import products.RecipeCandy;


public class Control {
  private static List<Candy> producedCandy = new ArrayList<>();
  private static List<Candy> packagedCandy = new ArrayList<>();
  private static Logger logger;

  public static void main(String[] args) {
    logger = Logger.getLogger("CandyFactory");
    setUpLogger(logger);

    CandyMachine.setRecipe("Kaseke", 5);
    CandyMachine.makeCandy(2000);
    CandyMachine.setRecipe("Emmake", 4);
    CandyMachine.makeCandy(5000);
    MedallionMachine.makeMedallions(1550);
    WrappingMachine.packageCandies();
    CandyMachine.setRecipe("Valge biison", 6);
    CandyMachine.makeCandy(4000);
    WrappingMachine.packageCandies();
  }

  private static void setUpLogger(Logger logger) {
    createLogIfNotExists();
    try {
      logger.addHandler(new FileHandler("/Users/JOOP_2017/Desktop", true));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void createLogIfNotExists() {
    File file = new File("/Users/JOOP_2017/Desktop");
    try {
      file.createNewFile();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void logCandies(Recipe recipe, Integer number) {
    Date date = new Date();
    logger.info("Production date: " + date + "; Recipe produced: " + recipe + "; Number produced: " + number);
  }

  public static void turnOn(Recipe recipe) throws Exception {
    if (recipe == null) {
      throw new Exception("No recipe!");
    }
    logger.info("Turning on. Recipe in use: " + recipe);
  }

  public static class MedallionMachine extends Control {

    public static String picture;

    public static void makeMedallions(Integer number) {
      try {
        turnOn(Recipe.medallionRecipe());
        for (int i = 0; i < number; i++) {
          newMedallion();
        }
        logCandies(Recipe.medallionRecipe(), number);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    private static void newMedallion() {
      Medallion medallion = new Medallion();
      if (picture != null) {
        medallion.setPicture(picture);
      }
      producedCandy.add(medallion);
    }

    private static void setPicture(String pictureIn) {
      picture = pictureIn;
    }
  }

  public static class CandyMachine extends Control {

    public static Recipe recipe;

    public static void setRecipe(String recipeName, Integer weight) {
      recipe = new Recipe(recipeName, weight);
    }

    public static void makeCandy(Integer number) {
      try {
        turnOn(recipe);
        for (int i = 0; i < number; i++) {
          newCandy();
        }
        logCandies(recipe, number);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    private static void newCandy() {
      RecipeCandy candy = new RecipeCandy();
      candy.setRecipe(recipe);
      producedCandy.add(candy);
    }
  }

  public static class WrappingMachine extends Control {

    public static void packageCandies() {
      logger.info("Starting wrapping candies.");
      for (Candy candy : producedCandy) {
        candy.wrap();
        packagedCandy.add(candy);
      }
      producedCandy.clear();
      logger.info("Packaged " + packagedCandy.size() + " candies.");
    }
  }

}
