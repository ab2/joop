package main;



public class Recipe {

  private String name;
  private Integer weight;

  public Recipe(String name, Integer weight) {
    this.name = name;
    this.weight = weight;
  }

  public static Recipe medallionRecipe() {
    return new Recipe("Medallion", 10);
  }

  @Override
  public String toString() {
    return "Recipe: " + this.name + ", " + weight + "g";
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }
}
